<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 10:48 AM
 */

namespace Importer\Factories;

use Importer\Contracts\Factory;
use Importer\Utils\Arr;

class Connection extends Base implements Factory
{

    public function create($name)
    {
        $c = Arr::get($this->config, $name, null);
        if ($c) {
            if ($c instanceof \Closure) {
                return $c();
            }
            $cls = Arr::get($c, 'cls');
            if (!$cls) {
                throw new \InvalidArgumentException("'cls' is a required parameter to create a connection.");
            }
            $inst = new $cls($this->logger);
            $args = Arr::get($c, 'args', []);
            return call_user_func_array([$inst, 'connect'], $args);
        }
    }
}
