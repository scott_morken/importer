<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 11:13 AM
 */

namespace Importer\Factories;

use Importer\Contracts\Factory;
use Importer\Utils\Arr;
use Pimple\Exception\UnknownIdentifierException;

class Action extends Base implements Factory
{

    /**
     * @param $name
     * @return mixed
     */
    public function create($name)
    {
        if ($name === 'writers') {
            Arr::set($this->instances, 'writers', $this->buildWriters());
            return Arr::get($this->instances, 'writers', []);
        }
        $created = [
            'pre'  => null,
            'act'  => null,
            'post' => null,
        ];
        $c = Arr::get($this->config, $name, null);
        if ($c) {
            if ($c instanceof \Closure) {
                return $c();
            }
            $backend = $this->getBackend(Arr::get($c, 'backend', null));
            foreach ($created as $k => $i) {
                $writer_info = Arr::get($c, $k, []);
                $interaction = Arr::get($writer_info, 'interaction', null);
                $created[$k] = $this->createType($writer_info, $backend, $interaction);
            }
        }
        return $created;
    }

    protected function buildWriters()
    {
        $built = [];
        $writers = Arr::get($this->config, 'writers', []);
        foreach ($writers as $name => $data) {
            $built[$name] = $this->create(sprintf('writers.%s', $name));
        }
        return $built;
    }

    protected function createType($config, $backend, $interaction)
    {
        if ($config) {
            $cls = Arr::get($config, 'cls');
            if (!$cls) {
                throw new \InvalidArgumentException("'cls' is a required argument to create an action.");
            }
            if (is_object($cls)) {
                $inst = $cls;
            } else {
                $inst = new $cls($this->getLogger(), $backend, $interaction);
            }
            $this->handleMethods($inst, Arr::get($config, 'methods', []));
            return $inst;
        }
        return null;
    }

    protected function getBackend($backend)
    {
        if ($backend) {
            try {
                $bf = $this->getContainer()['factory.backends'];
                return $bf->get($backend);
            } catch (UnknownIdentifierException $e) {
                try {
                    return $this->getContainer()[$backend];
                } catch (\Exception $e) {
                    throw $e;
                }
            }
        }
        return null;
    }

    protected function handleMethods($action, $methods)
    {
        foreach ($methods as $m => $args) {
            call_user_func_array([$action, $m], $args);
        }
    }
}
