<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 10:48 AM
 */

namespace Importer\Factories;

use Importer\Contracts\Factory;
use Importer\Utils\Arr;

class Backend extends Base implements Factory
{

    public function create($name)
    {
        $c = Arr::get($this->config, $name, null);
        if ($c) {
            if ($c instanceof \Closure) {
                return $c();
            }
            $cls = Arr::get($c, 'cls');
            if (!$cls) {
                throw new \InvalidArgumentException("'cls' is a required parameter to create a connection.");
            }
            $args = $this->parseArgs(Arr::get($c, 'args', []));
            $rc = new \ReflectionClass($cls);
            return $rc->newInstanceArgs($args);
        }
    }

    protected function parseArgs($args)
    {
        $new_args = [
            $this->logger,
        ];
        $cf = $this->getContainer()['factory.connections'];
        foreach ($args as $i => $arg) {
            if (is_array($arg)) {
                if (isset($arg['inject'])) {
                    $new_args[] = $this->getContainer()[$arg['inject']];
                }
                if (isset($arg['connection'])) {
                    $new_args[] = $cf->get($arg['connection']);
                }
            } else {
                $new_args[] = $arg;
            }
        }
        return $new_args;
    }
}
