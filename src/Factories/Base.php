<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 10:49 AM
 */

namespace Importer\Factories;

use Importer\Contracts\Factory;
use Pimple\Container;
use Psr\Log\LoggerInterface;

abstract class Base implements Factory
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var Container
     */
    protected $container;

    protected $instances = [];

    protected $config = [];

    public function __construct(Container $container, LoggerInterface $logger, $config = [])
    {
        $this->container = $container;
        $this->logger = $logger;
        $this->config = $config;
    }

    public function get($name)
    {
        if (!isset($this->instances[$name]) || is_null($this->instances[$name])) {
            $this->instances[$name] = $this->create($name);
        }
        return $this->instances[$name];
    }

    public function getContainer()
    {
        return $this->container;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }
}
