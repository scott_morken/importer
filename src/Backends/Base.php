<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 2:34 PM
 */

namespace Importer\Backends;

use Psr\Log\LoggerInterface;

abstract class Base
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    protected $backend;

    /**
     * Base constructor.
     *
     * @param \Psr\Log\LoggerInterface $logger
     * @param mixed|null               $backend
     */
    public function __construct(LoggerInterface $logger, $backend = null)
    {
        $this->logger = $logger;
        $this->backend = $backend;
    }

    /**
     * @return mixed
     */
    public function getBackend()
    {
        return $this->backend;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }
}
