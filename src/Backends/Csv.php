<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 1:40 PM
 */

namespace Importer\Backends;

use Importer\Contracts\Backend;
use Psr\Log\LoggerInterface;

class Csv extends Base implements Backend
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var string
     */
    protected $path;

    protected $fp;

    public function __construct(LoggerInterface $logger, $path)
    {
        parent::__construct($logger);
        $this->path = $path;
    }

    public function __destruct()
    {
        $this->close();
    }

    public function close()
    {
        $this->closeFile();
    }

    public function getBackend()
    {
        $type = func_get_arg(0) ?: 'r';
        return $this->openFile($type);
    }

    protected function closeFile()
    {
        if ($this->fp && is_resource($this->fp)) {
            fclose($this->fp);
            $this->fp = null;
        }
    }

    protected function openFile($type = 'r')
    {
        if (!$this->fp) {
            $this->fp = fopen($this->path, $type);
        }
        return $this->fp;
    }
}
