<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:25 AM
 */

namespace Importer\Backends;

use Importer\Contracts\Backend;

class PDO extends Base implements Backend
{

    public function __destruct()
    {
        $this->close();
    }

    /**
     * @return void
     */
    public function close()
    {
        $this->backend = null;
    }
}
