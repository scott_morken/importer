<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 12:50 PM
 */

namespace Importer;

use Monolog\Logger;
use Pimple\Container;
use Psr\Log\LoggerInterface;

class Bootstrap
{

    protected $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function bootstrap()
    {
        $this->registerPaths();
        $this->registerConfig();
        $this->registerLoggerBase();
        $this->registerErrorHandler();
        $this->addHandlersToLogger();
        $this->registerService();
        $this->registerFactories();
    }

    public static function register(Container $container)
    {
        $i = new static($container);
        $i->bootstrap();
        return $i;
    }

    protected function addHandlersToLogger()
    {
        $logger = $this->container[LoggerInterface::class];
        $conf = $this->container[\Importer\Contracts\Config::class];
        $handlers_config = $conf->get('logging.handlers', []);
        foreach ($handlers_config as $handler => $args) {
            $rc = new \ReflectionClass($handler);
            $logger->pushHandler($rc->newInstanceArgs($args));
        }
    }

    protected function registerConfig()
    {
        $this->container[\Importer\Contracts\Config::class] = function ($c) {
            $cp = $c['config_path'];
            $c = new Config([]);
            $fsi = new \FilesystemIterator($cp);
            foreach ($fsi as $info) {
                if ($info->getExtension() === 'php') {
                    $c->merge(require $info->getPathname());
                }
            }
            return $c;
        };
    }

    protected function registerErrorHandler()
    {
        \Monolog\ErrorHandler::register($this->container[\Psr\Log\LoggerInterface::class]);
    }

    protected function registerFactories()
    {
        $this->container['factory.connections'] = function ($c) {
            $logger = $c[\Psr\Log\LoggerInterface::class];
            $conf = $c[\Importer\Contracts\Config::class];
            return new \Importer\Factories\Connection($c, $logger, $conf->get('connections', []));
        };

        $this->container['factory.backends'] = function ($c) {
            $logger = $c[\Psr\Log\LoggerInterface::class];
            $conf = $c[\Importer\Contracts\Config::class];
            return new \Importer\Factories\Backend($c, $logger, $conf->get('backends', []));
        };

        $this->container['factory.actions'] = function ($c) {
            $logger = $c[\Psr\Log\LoggerInterface::class];
            $conf = $c[\Importer\Contracts\Config::class];
            return new \Importer\Factories\Action($c, $logger, $conf->get('actions', []));
        };
    }

    protected function registerLoggerBase()
    {
        $this->container[\Psr\Log\LoggerInterface::class] = function ($c) {
            $config = $c[\Importer\Contracts\Config::class];
            $bp = $c['storage_path'] . DIRECTORY_SEPARATOR . 'importer.log';
            $handlers = [
                new \Monolog\Handler\RotatingFileHandler(
                    $bp,
                    $config->get('logging.max_files', 7),
                    $config->get('logging.level', Logger::DEBUG)
                ),
            ];
            return new \Monolog\Logger('importer', $handlers);
        };
    }

    protected function registerPaths()
    {
        $base = realpath(__DIR__ . sprintf('%s..%s', DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR));
        $this->container['base_path'] = $base;
        $cp = $base . DIRECTORY_SEPARATOR . 'config';
        $this->verifyPath($cp);
        $this->container['config_path'] = realpath($cp);
        $sp = $base . DIRECTORY_SEPARATOR . 'storage';
        $this->verifyPath($sp);
        $this->container['storage_path'] = realpath($sp);
    }

    protected function verifyPath($path)
    {
        if (!file_exists($path)) {
            if (!mkdir($path, 0750, true)) {
                throw new ImporterException('Bootstrap: unable to create path: ' . $path);
            }
        }
    }

    protected function registerService()
    {
        $this->container[\Importer\Contracts\Service::class] = function ($c) {
            $bp = $c['storage_path'];
            $logger = $c[\Psr\Log\LoggerInterface::class];
            return new \Importer\Service($logger, $bp);
        };
    }
}
