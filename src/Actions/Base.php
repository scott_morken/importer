<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:08 AM
 */

namespace Importer\Actions;

use Importer\Contracts\Backend;
use Psr\Log\LoggerInterface;

abstract class Base implements \Importer\Contracts\Actions\Base
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var Backend
     */
    protected $backend;

    /**
     * @var mixed
     */
    protected $interaction;

    protected $interaction_required = true;

    /**
     * Base constructor.
     *
     * @param \Psr\Log\LoggerInterface         $logger
     * @param \Importer\Contracts\Backend|null $backend
     * @param null                             $interaction
     */
    public function __construct(LoggerInterface $logger, Backend $backend = null, $interaction = null)
    {
        $this->logger = $logger;
        $this->setBackend($backend);
        $this->setInteraction($interaction);
    }

    /**
     * @return Backend
     */
    public function getBackend()
    {
        return $this->backend;
    }

    /**
     * @param \Importer\Contracts\Backend $backend
     * @return void
     */
    public function setBackend(Backend $backend)
    {
        $this->backend = $backend;
    }

    /**
     * @return mixed|null
     */
    public function getInteraction()
    {
        if (!$this->interaction && $this->interaction_required) {
            throw new ActionException('No interaction provided.');
        }
        return $this->interaction;
    }

    /**
     * @param $interaction
     * @return void
     */
    public function setInteraction($interaction)
    {
        $this->interaction = $interaction;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }
}
