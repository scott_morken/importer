<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:56 AM
 */

namespace Importer\Actions;

use Importer\Contracts\Data;

abstract class Write extends Base implements \Importer\Contracts\Actions\Write
{

    protected $map = [];

    /**
     * @return array
     */
    public function getMap()
    {
        return $this->map;
    }

    /**
     * @param $map
     * @return void
     */
    public function setMap($map)
    {
        $this->map = $map;
    }

    protected function mapColumns(Data $data, $columns)
    {
        $put = [];
        if (empty($columns)) {
            return $data->toArray();
        }
        foreach ($columns as $to => $from) {
            if ($from instanceof \Closure) {
                $f = $from($data);
            } else {
                $f = $data->get($from);
            }
            $put[$to] = $f;
        }
        return $put;
    }
}
