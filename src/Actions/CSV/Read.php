<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:49 AM
 */

namespace Importer\Actions\CSV;

class Read extends \Importer\Actions\Read implements \Importer\Contracts\Actions\Read
{

    protected $interaction_required = false;

    /**
     * @param array $params
     * @return \Importer\Contracts\Data[]
     */
    public function read($params = [])
    {
        $count = 0;
        $this->getLogger()
             ->debug('Preparing READ from CSV.', $params);
        $fp = $this->getBackend()
                   ->getBackend('r');
        while (($data = fgetcsv($fp)) !== false) {
            $count++;
            yield $this->getData($data);
        }
        $fp = null;
        $this->getBackend()
             ->close();
        $this->getLogger()
             ->debug("Read $count rows.");
    }
}
