<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:54 AM
 */

namespace Importer\Actions\CSV;

class Write extends \Importer\Actions\Write implements \Importer\Contracts\Actions\Write
{

    protected $interaction_required = false;

    /**
     * @param array $data
     * @return int
     */
    public function write(array $data)
    {
        $fp = $this->getBackend()
                   ->getBackend('a');
        $count = 0;
        foreach ($data as $d) {
            $put = $this->mapColumns($d, $this->getMap());
            if (!fputcsv($fp, $put)) {
                $this->logger->error('Unable to write CSV line to file.');
            } else {
                $count++;
            }
        }
        return $count;
    }
}
