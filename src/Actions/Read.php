<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:55 AM
 */

namespace Importer\Actions;

use Importer\Data;

abstract class Read extends Base implements \Importer\Contracts\Actions\Read
{

    /**
     * @param $attributes
     * @return \Importer\Contracts\Data
     */
    public function getData($attributes)
    {
        return new Data($attributes);
    }
}
