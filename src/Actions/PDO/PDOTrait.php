<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 8:35 AM
 */

namespace Importer\Actions\PDO;

use Importer\Actions\ActionException;

trait PDOTrait
{

    /**
     * @param \PDO  $pdo
     * @param       $sql
     * @param array $params
     * @param bool  $count
     * @return mixed
     * @throws \Importer\Actions\ActionException
     */
    protected function executeSql(\PDO $pdo, $sql, $params = [], $count = false)
    {
        $this->getLogger()
             ->debug("Executing: [ $sql ].", $params);
        $st = $this->getStatement($pdo, $sql);
        return $this->executeWithParams($st, $params, $count);
    }

    protected function executeWithParams(\PDOStatement $st, $params, $count = false)
    {
        if (!empty($params)) {
            $r = $st->execute($params);
        } else {
            $r = $st->execute();
        }
        if ($count) {
            return $st->rowCount();
        }
        return $r;
    }

    protected function getErrorInfo(\PDO $pdo)
    {
        $info = $pdo->errorInfo();
        return sprintf('%s: %s', $info[0], $info[2]);
    }

    /**
     * @param \PDO $pdo
     * @param      $sql
     * @return \PDOStatement
     * @throws \Importer\Actions\ActionException
     */
    protected function getStatement(\PDO $pdo, $sql)
    {
        $st = $pdo->prepare($sql);
        if (!$st) {
            throw new ActionException("Unable to prepare statement [ $sql ]: " . $this->getErrorInfo($pdo));
        }
        return $st;
    }
}
