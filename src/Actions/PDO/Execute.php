<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:08 AM
 */

namespace Importer\Actions\PDO;

class Execute extends \Importer\Actions\Execute implements \Importer\Contracts\Actions\Execute
{

    use PDOTrait;

    /**
     * @param array $params
     * @return mixed
     * @throws \Importer\Actions\ActionException
     */
    public function execute($params = [])
    {
        $pdo = $this->getBackend()
                    ->getBackend();
        $sql = $this->getInteraction();
        if (is_array($sql)) {
            $results = [];
            foreach ($sql as $s) {
                $results[] = $this->executeSql($pdo, $s, $params);
            }
            return $results;
        }
        return $this->executeSql($pdo, $sql, $params);
    }
}
