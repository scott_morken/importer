<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:31 AM
 */

namespace Importer\Actions\PDO;

class Write extends \Importer\Actions\Write implements \Importer\Contracts\Actions\Write
{

    use PDOTrait;

    /**
     * @param array $data
     * @return int
     * @throws \Importer\Actions\ActionException
     */
    public function write(array $data)
    {
        $inserted = 0;
        $sql = $this->getInteraction();
        $sql .= $this->createPlaceholders($data);
        $pdo = $this->getBackend()
                    ->getBackend();
        $params = $this->getParameters($data);
        $inserted += $this->executeSql($pdo, $sql, $params, true);
        return $inserted;
    }

    protected function createPlaceholders($data)
    {
        $ph = [];
        $count = count($this->getMap());
        foreach ($data as $i => $d) {
            if ($count === 0) {
                $count = count($d->toArray());
            }
            $internal = array_fill(0, $count, '?');
            $ph[$i] = sprintf('(%s)', implode(', ', $internal));
        }
        return sprintf(' VALUES %s', implode(",\n", $ph));
    }

    protected function getParameters(array $data)
    {
        $params = [];
        $columns = $this->getMap();
        foreach ($data as $i => $d) {
            $put = array_values($this->mapColumns($d, $columns));
            $params = array_merge($params, $put);
        }
        return $params;
    }
}
