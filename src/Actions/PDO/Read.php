<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:37 AM
 */

namespace Importer\Actions\PDO;

class Read extends \Importer\Actions\Read implements \Importer\Contracts\Actions\Read
{

    use PDOTrait;

    /**
     * @param array $params
     * @return \Importer\Contracts\Data[]
     * @throws \Importer\Actions\ActionException
     */
    public function read($params = [])
    {
        $count = 0;
        $sql = $this->getInteraction();
        $this->getLogger()
             ->debug('Preparing READ for: ' . $sql, $params);
        $st = $this->getStatement(
            $this->getBackend()
                 ->getBackend(),
            $sql
        );
        $this->executeWithParams($st, $params);
        while (($data = $st->fetch(\PDO::FETCH_ASSOC)) !== false) {
            $count++;
            yield $this->getData($data);
        }
        $st->closeCursor();
        $this->getLogger()
             ->debug("Read $count rows.");
    }
}
