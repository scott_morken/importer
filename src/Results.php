<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 1:59 PM
 */

namespace Importer;

use Importer\Utils\Arr;

class Results
{

    protected $results = [
        'start'   => null,
        'pre'     => [],
        'act'     => [
            'reader' => 0,
        ],
        'post'    => [],
        'stop'    => null,
        'elapsed' => null,
    ];

    public function __get($key)
    {
        return $this->getType($key);
    }

    public function increment($type, $name, $count)
    {
        $v = $this->getName($type, $name, 0);
        $v = $v + $count;
        $this->results[$type][$name] = $v;
    }

    public function set($type, $name, $data)
    {
        $v = $this->getName($type, $name, null);
        $this->results[$type][$name] = $data;
    }

    public function start()
    {
        $this->results['start'] = microtime(true);
    }

    public function stop()
    {
        $this->results['stop'] = microtime(true);
        $this->results['elapsed'] = ($this->results['stop'] - $this->results['start']);
    }

    public function toArray()
    {
        return $this->results;
    }

    protected function getName($type, $name, $default = null)
    {
        $t = $this->getType($type);
        if (!Arr::has($t, $name)) {
            $this->results[$type][$name] = $default;
        }
        return $this->results[$type][$name];
    }

    protected function getType($type)
    {
        if (!Arr::has($this->results, $type)) {
            $this->results[$type] = [];
        }
        return $this->results[$type];
    }
}
