<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 10:57 AM
 */

namespace Importer;

use Importer\Utils\Arr;

class Config implements \Importer\Contracts\Config
{

    protected $config = [];

    public function __construct(array $config)
    {
        $this->config = $config;
    }

    public function get($key, $default = null)
    {
        return Arr::get($this->config, $key, $default);
    }

    public function merge(array $config)
    {
        $this->config = array_merge_recursive($this->config, $config);
    }

    public function set($key, $value)
    {
        Arr::set($config, $key, $value);
    }
}
