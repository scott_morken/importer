<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 2:03 PM
 */

namespace Importer\Db\Connections;

use Importer\Contracts\Db\Connection;
use Psr\Log\LoggerInterface;

class PDOConnection implements Connection
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    protected $pdo_cls = \PDO::class;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param                          $dsn
     * @param                          $user
     * @param                          $password
     * @param array                    $options
     * @return mixed
     */
    public function connect($dsn, $user, $password, $options = [])
    {
        try {
            $c = $this->pdo_cls;
            return new $c($dsn, $user, $password, $options);
        } catch (\PDOException $e) {
            $this->getLogger()
                 ->error($e);
        }
        return false;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }
}
