<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 11:40 AM
 */

namespace Importer;

use Psr\Log\LoggerInterface;

class Service implements \Importer\Contracts\Service
{

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @var string
     */
    protected $file;

    protected $should_remove = true;

    public function __construct(LoggerInterface $logger, $path, $name = 'service.lock')
    {
        $this->validEnvironment($path);
        $this->logger = $logger;
        $this->file = $path . DIRECTORY_SEPARATOR . $name;
    }

    public function __destruct()
    {
        $this->stop();
    }

    public function isRunning()
    {
        $this->logger->debug('Checking status...');
        if (file_exists($this->file)) {
            $this->logger->debug('* Service file exists.');
            return true;
        }
        $this->logger->debug('* Service file does not exist.');
        return false;
    }

    public function start()
    {
        $this->logger->debug('Attempting to start service.');
        if ($this->isRunning()) {
            $this->should_remove = false;
            throw new ServiceException("Service already running.");
        }
        if ($this->createFile()) {
            $this->logger->debug('Service started.');
            return true;
        } else {
            $this->logger->debug('Unable to create service file.');
        }
        return false;
    }

    public function stop()
    {
        $pid = $this->isRunning();
        return $this->stopService($pid);
    }

    protected function createFile()
    {
        return touch($this->file);
    }

    protected function removeFile()
    {
        if (file_exists($this->file) && $this->should_remove) {
            $this->logger->debug('Removing service file.');
            unlink($this->file);
        }
    }

    protected function stopService($pid)
    {
        $this->logger->debug('Stopping service.');
        $this->removeFile();
    }

    protected function validEnvironment($path)
    {
        if (!is_writable($path)) {
            throw new ServiceException("$path is not writable.");
        }
    }
}
