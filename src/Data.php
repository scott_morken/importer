<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 1:54 PM
 */

namespace Importer;

use Importer\Utils\Arr;

class Data implements \Importer\Contracts\Data
{

    protected $attributes = [];

    public function __construct($attributes = [])
    {
        $this->setAttributes($attributes);
    }

    public function get($key, $default = null)
    {
        return Arr::get($this->attributes, $key, $default);
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function setAttributes($attributes = [])
    {
        foreach ($attributes as $k => $v) {
            $this->set($k, $v);
        }
    }

    public function set($key, $value)
    {
        Arr::set($this->attributes, $key, $value);
    }

    public function toArray()
    {
        return $this->getAttributes();
    }
}
