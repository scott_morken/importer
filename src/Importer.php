<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 1:22 PM
 */

namespace Importer;

use Importer\Contracts\Actions\Base;
use Importer\Contracts\Actions\Execute;
use Importer\Contracts\Actions\Read;
use Importer\Contracts\Actions\Write;
use Importer\Utils\Arr;
use Pimple\Container;
use Psr\Log\LoggerInterface;

class Importer
{

    /**
     * @var \Pimple\Container
     */
    protected $container;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    protected $reader;

    protected $writers = [];

    protected $results;

    public function __construct(Container $container, LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->container = $container;
    }

    public function close()
    {
        $this->attemptClose($this->getReader());
        foreach ($this->getWriters() as $writer) {
            $this->attemptClose($writer);
        }
    }

    public function getContainer()
    {
        return $this->container;
    }

    public function getResults()
    {
        if (!$this->results) {
            $this->results = new Results();
        }
        return $this->results;
    }

    public function run($chunk = 50)
    {
        $this->getResults()
             ->start();
        $this->pre();
        $this->act($chunk);
        $this->post();
        $this->close();
        $this->getResults()
             ->stop();
        return $this->getResults();
    }

    protected function act($chunk)
    {
        $writers = $this->getWriters();
        $reader = $this->getReader();
        $ract = $reader['act'];
        $method = $this->getMethod($ract);
        $current = [];
        foreach ($ract->$method() as $i => $data) {
            $this->incrementResult('act', 'reader', 1);
            $count_curr = count($current);
            if ($count_curr && ($count_curr % $chunk === 0)) {
                $this->writeChunk($current, $writers);
                $current = [];
            }
            $current[] = $data;
        }
        if ($current) {
            $this->writeChunk($current, $writers);
        }
    }

    protected function addSimpleResult($type, $name, $result)
    {
        $this->getResults()
             ->set($type, $name, $result);
    }

    protected function attemptClose($actions)
    {
        foreach ($actions as $i => $action) {
            if ($action && method_exists($action, 'close')) {
                $action->close();
            }
        }
    }

    protected function getActionableWriters($writers)
    {
        $actionable = [];
        foreach ($writers as $name => $writer) {
            $wact = $writer['act'];
            if ($wact) {
                $actionable[$name] = $wact;
            }
        }
        return $actionable;
    }

    protected function getMethod(Base $action)
    {
        if ($action instanceof Execute) {
            return 'execute';
        }
        if ($action instanceof Read) {
            return 'read';
        }
        if ($action instanceof Write) {
            return 'write';
        }
        throw new ImporterException(sprintf('%s is not a recognized action.', get_class($action)));
    }

    protected function getReader()
    {
        if (!$this->reader) {
            $af = $this->getContainer()['factory.actions'];
            $r = $af->get('reader');
            $this->verifyAction($r);
            $this->reader = $r;
        }
        return $this->reader;
    }

    protected function getWriters()
    {
        if (!$this->writers) {
            $af = $this->getContainer()['factory.actions'];
            $writers = $af->get('writers');
            if (!$writers) {
                throw new ImporterException('No writers loaded.');
            }
            foreach ($writers as $w) {
                $this->verifyAction($w, 'writer');
            }
            $this->writers = $writers;
        }
        return $this->writers;
    }

    protected function incrementResult($type, $name, $count)
    {
        $this->getResults()
             ->increment($type, $name, $count);
    }

    protected function post()
    {
        $this->preOrPost('post');
    }

    protected function pre()
    {
        $this->preOrPost('pre');
    }

    protected function preOrPost($type)
    {
        $reader = $this->getReader();
        $this->addSimpleResult($type, 'reader', $this->runAction($type, $reader));
        $writers = $this->getWriters();
        foreach ($writers as $name => $writer) {
            $this->addSimpleResult($type, 'writers.' . $name, $this->runAction($type, $writer));
        }
    }

    protected function runAction($type, $actions, $args = [])
    {
        $action = Arr::get($actions, $type, null);
        if ($action) {
            $method = $this->getMethod($action);
            return call_user_func_array([$action, $method], $args);
        }
    }

    protected function verifyAction($action, $type = 'reader')
    {
        if (is_array($action) && isset($action['act'])) {
            if ($action['act'] instanceof Base) {
                return true;
            }
        }
        throw new ImporterException(sprintf('Could not create a %s.', $type));
    }

    protected function writeChunk($data, array $writers)
    {
        foreach ($writers as $name => $writer) {
            $this->incrementResult('act', 'writers.' . $name, $this->runAction('act', $writer, [$data]));
        }
    }
}
