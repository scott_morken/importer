<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 6:45 AM
 */

namespace Importer\Contracts;

interface Actionable
{

    /**
     * @param $type
     * @return mixed
     */
    public function getAction($type);

    /**
     * @return array
     */
    public function getActions();

    /**
     * @return \Importer\Contracts\Backend
     */
    public function getBackend();

    /**
     * @param array $actions
     * @return void
     */
    public function setActions(array $actions);
}
