<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 11:17 AM
 */

namespace Importer\Contracts;

interface Backend
{

    /**
     * @return void
     */
    public function close();

    /**
     * @return mixed
     */
    public function getBackend();

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger();
}
