<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 11:18 AM
 */

namespace Importer\Contracts;

interface Data
{

    public function get($key, $default = null);

    public function getAttributes();

    public function set($key, $value);

    public function setAttributes($attributes = []);

    public function toArray();
}
