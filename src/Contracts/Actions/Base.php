<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:02 AM
 */

namespace Importer\Contracts\Actions;

use Importer\Contracts\Backend;

interface Base
{

    /**
     * @return Backend
     */
    public function getBackend();

    /**
     * @return mixed|null
     */
    public function getInteraction();

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger();

    /**
     * @param \Importer\Contracts\Backend $backend
     * @return void
     */
    public function setBackend(Backend $backend);

    /**
     * @param $interaction
     * @return void
     */
    public function setInteraction($interaction);
}
