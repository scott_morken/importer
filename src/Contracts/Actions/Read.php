<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:01 AM
 */

namespace Importer\Contracts\Actions;

interface Read extends Base
{

    /**
     * @param $attributes
     * @return \Importer\Contracts\Data
     */
    public function getData($attributes);

    /**
     * @param array $params
     * @return \Importer\Contracts\Data[]
     */
    public function read($params = []);
}
