<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:01 AM
 */

namespace Importer\Contracts\Actions;

interface Execute extends Base
{

    /**
     * @param array $params
     * @return mixed
     */
    public function execute($params = []);
}
