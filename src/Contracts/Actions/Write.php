<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:03 AM
 */

namespace Importer\Contracts\Actions;

interface Write extends Base
{

    /**
     * @return array
     */
    public function getMap();

    /**
     * @param $map
     * @return void
     */
    public function setMap($map);

    /**
     * @param array $data
     * @return int
     */
    public function write(array $data);
}
