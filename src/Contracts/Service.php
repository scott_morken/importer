<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 11:39 AM
 */

namespace Importer\Contracts;

interface Service
{

    public function isRunning();

    public function start();

    public function stop();
}
