<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 10:47 AM
 */

namespace Importer\Contracts;

interface Factory
{

    /**
     * @param $name
     * @return mixed
     */
    public function create($name);

    /**
     * @param $name
     * @return mixed
     */
    public function get($name);

    /**
     * @return \Pimple\Container
     */
    public function getContainer();

    /**
     * @return \Psr\Log\LoggerInterface
     */
    public function getLogger();
}
