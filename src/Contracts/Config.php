<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 10:59 AM
 */

namespace Importer\Contracts;

interface Config
{

    /**
     * @param string $key
     * @param null   $default
     * @return mixed
     */
    public function get($key, $default = null);

    /**
     * @param array $config
     * @return void
     */
    public function merge(array $config);

    /**
     * @param string $key
     * @param mixed  $value
     * @return mixed
     */
    public function set($key, $value);
}
