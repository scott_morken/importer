<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 1:14 PM
 */
return [
    'logging' => [
        'level'     => \Monolog\Logger::DEBUG,
        'max_files' => 7,
        'handlers'  => [
            \Monolog\Handler\StreamHandler::class       => [
                'php://stdout',
                \Monolog\Logger::NOTICE,
            ],
            \Monolog\Handler\NativeMailerHandler::class => [
                'email@domain.edu',
                'Importer Results',
                'no-reply@importer.domain.edu',
                \Monolog\Logger::NOTICE,
            ],
        ],
    ],
];
