<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 1:13 PM
 */
return [
    'connections' => [
        'sqlite' => [
            'cls'  => \Importer\Db\Connections\PDOConnection::class,
            'args' => [
                'dsn'      => 'sqlite::memory:',
                'username' => null,
                'password' => null,
            ],
        ],
    ],
];
