<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 12:28 PM
 */

namespace Tests\Importer\integration;

use Importer\Service;
use Importer\ServiceException;
use Monolog\Handler\NullHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class ServiceTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . 'service.lock')) {
            unlink(__DIR__ . DIRECTORY_SEPARATOR . 'service.lock');
        }
    }

    public function testSimpleServiceNotStarted()
    {
        $sut = $this->getSut();
        $this->assertFalse($sut->isRunning());
        $this->assertFileNotExists(__DIR__ . DIRECTORY_SEPARATOR . 'service.lock');
    }

    public function testSimpleServiceStarts()
    {
        $sut = $this->getSut();
        $this->assertTrue($sut->start());
        $this->assertTrue($sut->isRunning());
        $this->assertFileExists(__DIR__ . DIRECTORY_SEPARATOR . 'service.lock');
        $sut->stop();
        $this->assertFileNotExists(__DIR__ . DIRECTORY_SEPARATOR . 'service.lock');
    }

    public function testExistingFileIsException()
    {
        touch(__DIR__ . DIRECTORY_SEPARATOR . 'service.lock');
        $sut = $this->getSut();
        $this->expectException(ServiceException::class);
        $this->expectExceptionMessage('Service already running.');
        $sut->start();
    }

    protected function getSut()
    {
        $handlers = [new NullHandler()];
        $l = new Logger((new \ReflectionClass($this))->getShortName(), $handlers);
        return new Service($l, __DIR__);
    }
}