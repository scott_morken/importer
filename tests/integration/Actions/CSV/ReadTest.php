<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 10:17 AM
 */

namespace Tests\Importer\integration\Actions\CSV;

use Importer\Actions\CSV\Read;
use Importer\Backends\Csv;
use Monolog\Handler\NullHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Mockery as m;

class ReadTest extends TestCase
{

    protected $path = __DIR__ . '/read.csv';

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testRead()
    {
        $sut = $this->getSut();
        foreach ($sut->read() as $i => $data) {
            $this->assertEquals('bar ' . $i, $data->get(0));
            $this->assertEquals('buz ' . $i, $data->get(1));
        }
        $this->assertEquals(1, $i);
    }

    protected function getSut()
    {
        $handlers = [new NullHandler()];
        $l = new Logger((new \ReflectionClass($this))->getShortName(), $handlers);
        $b = new Csv($l,$this->path);
        $sut = new Read($l, $b);
        return $sut;
    }
}