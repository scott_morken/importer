<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 10:23 AM
 */

namespace Tests\Importer\integration\Actions\CSV;

use Importer\Actions\CSV\Write;
use Importer\Backends\Csv;
use Importer\Data;
use Monolog\Handler\NullHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class WriteTest extends TestCase
{

    protected $path = __DIR__ . '/write.csv';

    public function tearDown()
    {
        parent::tearDown();
        if (file_exists($this->path)) {
            unlink($this->path);
        }
    }

    public function testWrite()
    {
        $sut = $this->getSut();
        $data = [
            new Data(['foo' => 'bar', 'biz' => 'buz']),
        ];
        $r = $sut->write($data);
        $this->assertEquals(1, $r);
        $this->assertFileExists($this->path);
        $this->assertEquals("bar,buz" . PHP_EOL, file_get_contents($this->path));
    }

    public function testWriteWithMap()
    {
        $data = [
            new Data(['OTHERFOO' => 'bar', 'OTHERBIZ' => 'buz']),
        ];
        $sut = $this->getSut();
        $sut->setMap(['foo' => 'OTHERFOO', 'biz' => 'OTHERBIZ']);
        $this->assertEquals(1, $sut->write($data));
        $this->assertEquals("bar,buz" . PHP_EOL, file_get_contents($this->path));
    }

    public function testWriteWithMapLimitsColumns()
    {
        $data = [
            new Data(['foo' => 'bar', 'bar' => 'buz']),
        ];
        $sut = $this->getSut();
        $sut->setMap(['foo' => 'foo']);
        $this->assertEquals(1, $sut->write($data));
        $this->assertEquals("bar" . PHP_EOL, file_get_contents($this->path));
    }

    public function testWriteWithMapAndClosure()
    {
        $data = [
            new Data(['OTHERFOO' => 'bar', 'OTHERBIZ' => 'buz']),
        ];
        $sut = $this->getSut();
        $sut->setMap(
            [
                'foo' => 'OTHERFOO',
                'biz' => function ($d) {
                    return $d->get('OTHERBIZ') . ' closure';
                },
            ]
        );
        $this->assertEquals(1, $sut->write($data));
        $this->assertEquals("bar,\"buz closure\"" . PHP_EOL, file_get_contents($this->path));
    }

    public function testWriteMultipleRows()
    {
        $data = [
            new Data(['foo' => 'bar 1', 'biz' => 'buz 1']),
            new Data(['foo' => 'bar 2', 'biz' => 'buz 2']),
        ];
        $sut = $this->getSut();
        $this->assertEquals(2, $sut->write($data));
        $this->assertEquals('"bar 1","buz 1"' . PHP_EOL . '"bar 2","buz 2"' . PHP_EOL, file_get_contents($this->path));
    }

    public function testWriteMultipleRowsWithMap()
    {
        $data = [
            new Data(['OTHERFOO' => 'bar 1', 'OTHERBIZ' => 'buz 1']),
            new Data(['OTHERFOO' => 'bar 2', 'OTHERBIZ' => 'buz 2']),
        ];
        $sut = $this->getSut();
        $sut->setMap(['foo' => 'OTHERFOO', 'biz' => 'OTHERBIZ']);
        $this->assertEquals(2, $sut->write($data));
        $this->assertEquals('"bar 1","buz 1"' . PHP_EOL . '"bar 2","buz 2"' . PHP_EOL, file_get_contents($this->path));
    }

    protected function getSut()
    {
        $handlers = [new NullHandler()];
        $l = new Logger((new \ReflectionClass($this))->getShortName(), $handlers);
        $b = new Csv($l,$this->path);
        $sut = new Write($l, $b);
        return $sut;
    }
}