<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 1:10 PM
 */

namespace Tests\Importer\unit;

use Importer\Bootstrap;
use Importer\Contracts\Config;
use PHPUnit\Framework\TestCase;
use Pimple\Container;

class BootstrapTest extends TestCase
{

    public function testBootstrapFromStatic()
    {
        $c = new Container();
        $i = Bootstrap::register($c);
        $this->assertInstanceOf(Bootstrap::class, $i);
    }

    public function testBootstrapFromStaticIteratesConfigFiles()
    {
        $c = new Container();
        $i = Bootstrap::register($c);
        $config = $c[Config::class];
        $this->assertArrayHasKey('reader', $config->get('actions', []));
        $this->assertArrayHasKey('handlers', $config->get('logging', []));
    }
}