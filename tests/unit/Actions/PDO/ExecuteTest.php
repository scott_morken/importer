<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 8:49 AM
 */

namespace Tests\Importer\unit\Actions\PDO;

use Importer\Actions\ActionException;
use Importer\Actions\PDO\Execute;
use Importer\Backends\PDO;
use Monolog\Handler\NullHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Mockery as m;

class ExecuteTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testExecute()
    {
        list($sut, $pdo, $stmt) = $this->getSut();
        $pdo->shouldReceive('prepare')->once()->with('TRUNCATE something')->andReturn($stmt);
        $stmt->shouldReceive('execute')->once()->andReturn(true);
        $this->assertTrue($sut->execute());
    }

    public function testExecuteWithParams()
    {
        list($sut, $pdo, $stmt) = $this->getSut();
        $pdo->shouldReceive('prepare')->once()->with('TRUNCATE something')->andReturn($stmt);
        $stmt->shouldReceive('execute')->once()->with(['foo' => 'bar'])->andReturn(true);
        $this->assertTrue($sut->execute(['foo' => 'bar']));
    }

    public function testExecuteNoInteractionIsException()
    {
        list($sut, $pdo, $stmt) = $this->getSut(null);
        $this->expectException(ActionException::class);
        $this->expectExceptionMessage('No interaction provided.');
        $sut->execute();
    }

    protected function getSut($interaction = 'TRUNCATE something')
    {
        $pdo = m::mock(\PDO::class);
        $stmt = m::mock(\PDOStatement::class);
        $handlers = [new NullHandler()];
        $l = new Logger((new \ReflectionClass($this))->getShortName(), $handlers);
        $b = new PDO($l, $pdo);
        $sut = new Execute($l, $b, $interaction);
        return [$sut, $pdo, $stmt];
    }
}