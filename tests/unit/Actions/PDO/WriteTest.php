<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 9:52 AM
 */

namespace Tests\Importer\unit\Actions\PDO;

use Importer\Actions\PDO\Write;
use Importer\Backends\PDO;
use Importer\Data;
use Mockery as m;
use Monolog\Handler\NullHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class WriteTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testWrite()
    {
        $data = [
            new Data(['foo' => 'bar', 'biz' => 'buz']),
        ];
        list($sut, $pdo, $stmt) = $this->getSut();
        $pdo->shouldReceive('prepare')
            ->once()
            ->with("INSERT INTO something ('foo', 'biz') VALUES (?, ?)")
            ->andReturn($stmt);
        $stmt->shouldReceive('execute')
             ->once()
             ->with(['bar', 'buz']);
        $stmt->shouldReceive('rowCount')
             ->once()
             ->andReturn(1);
        $this->assertEquals(1, $sut->write($data));
    }

    public function testWriteWithMap()
    {
        $data = [
            new Data(['OTHERFOO' => 'bar', 'OTHERBIZ' => 'buz']),
        ];
        list($sut, $pdo, $stmt) = $this->getSut();
        $sut->setMap(['foo' => 'OTHERFOO', 'biz' => 'OTHERBIZ']);
        $pdo->shouldReceive('prepare')
            ->once()
            ->with("INSERT INTO something ('foo', 'biz') VALUES (?, ?)")
            ->andReturn($stmt);
        $stmt->shouldReceive('execute')
             ->once()
             ->with(['bar', 'buz']);
        $stmt->shouldReceive('rowCount')
             ->once()
             ->andReturn(1);
        $this->assertEquals(1, $sut->write($data));
    }

    public function testWriteWithMapLimitsColumns()
    {
        $data = [
            new Data(['foo' => 'bar', 'bar' => 'buz']),
        ];
        list($sut, $pdo, $stmt) = $this->getSut("INSERT INTO something ('foo')");
        $sut->setMap(['foo' => 'foo']);
        $pdo->shouldReceive('prepare')
            ->once()
            ->with("INSERT INTO something ('foo') VALUES (?)")
            ->andReturn($stmt);
        $stmt->shouldReceive('execute')
             ->once()
             ->with(['bar']);
        $stmt->shouldReceive('rowCount')
             ->once()
             ->andReturn(1);
        $this->assertEquals(1, $sut->write($data));
    }

    public function testWriteWithMapAndClosure()
    {
        $data = [
            new Data(['OTHERFOO' => 'bar', 'OTHERBIZ' => 'buz']),
        ];
        list($sut, $pdo, $stmt) = $this->getSut();
        $sut->setMap(
            [
                'foo' => 'OTHERFOO',
                'biz' => function ($d) {
                    return $d->get('OTHERBIZ') . ' closure';
                },
            ]
        );
        $pdo->shouldReceive('prepare')
            ->once()
            ->with("INSERT INTO something ('foo', 'biz') VALUES (?, ?)")
            ->andReturn($stmt);
        $stmt->shouldReceive('execute')
             ->once()
             ->with(['bar', 'buz closure']);
        $stmt->shouldReceive('rowCount')
             ->once()
             ->andReturn(1);
        $this->assertEquals(1, $sut->write($data));
    }

    public function testWriteMultipleRows()
    {
        $data = [
            new Data(['foo' => 'bar 1', 'biz' => 'buz 1']),
            new Data(['foo' => 'bar 2', 'biz' => 'buz 2']),
        ];
        list($sut, $pdo, $stmt) = $this->getSut();
        $pdo->shouldReceive('prepare')
            ->once()
            ->with("INSERT INTO something ('foo', 'biz') VALUES (?, ?),\n(?, ?)")
            ->andReturn($stmt);
        $stmt->shouldReceive('execute')
             ->once()
             ->with(['bar 1', 'buz 1', 'bar 2', 'buz 2']);
        $stmt->shouldReceive('rowCount')
             ->once()
             ->andReturn(2);
        $this->assertEquals(2, $sut->write($data));
    }

    public function testWriteMultipleRowsWithMap()
    {
        $data = [
            new Data(['OTHERFOO' => 'bar 1', 'OTHERBIZ' => 'buz 1']),
            new Data(['OTHERFOO' => 'bar 2', 'OTHERBIZ' => 'buz 2']),
        ];
        list($sut, $pdo, $stmt) = $this->getSut();
        $sut->setMap(['foo' => 'OTHERFOO', 'biz' => 'OTHERBIZ']);
        $pdo->shouldReceive('prepare')
            ->once()
            ->with("INSERT INTO something ('foo', 'biz') VALUES (?, ?),\n(?, ?)")
            ->andReturn($stmt);
        $stmt->shouldReceive('execute')
             ->once()
             ->with(['bar 1', 'buz 1', 'bar 2', 'buz 2']);
        $stmt->shouldReceive('rowCount')
             ->once()
             ->andReturn(2);
        $this->assertEquals(2, $sut->write($data));
    }

    protected function getSut($interaction = "INSERT INTO something ('foo', 'biz')")
    {
        $pdo = m::mock(\PDO::class);
        $stmt = m::mock(\PDOStatement::class);
        $handlers = [new NullHandler()];
        $l = new Logger((new \ReflectionClass($this))->getShortName(), $handlers);
        $b = new PDO($l, $pdo);
        $sut = new Write($l, $b, $interaction);
        return [$sut, $pdo, $stmt];
    }
}