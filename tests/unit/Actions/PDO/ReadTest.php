<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 9:35 AM
 */

namespace Tests\Importer\unit\Actions\PDO;

use Importer\Actions\ActionException;
use Importer\Actions\PDO\Read;
use Importer\Backends\PDO;
use Importer\Contracts\Data;
use Monolog\Handler\NullHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Mockery as m;

class ReadTest extends TestCase
{

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }

    public function testRead()
    {
        list($sut, $pdo, $stmt) = $this->getSut();
        $results = $this->getResults();
        $pdo->shouldReceive('prepare')->once()->with('SELECT * FROM something')->andReturn($stmt);
        $stmt->shouldReceive('execute')->once();
        $stmt->shouldReceive('fetch')->andReturnValues($results);
        $stmt->shouldReceive('closeCursor')->once();
        foreach ($sut->read() as $i => $d) {
            $this->assertInstanceOf(Data::class, $d);
            $this->assertEquals($results[$i], $d->toArray());
        }
        $this->assertEquals(1, $i);
    }

    public function testReadWithParams()
    {
        list($sut, $pdo, $stmt) = $this->getSut();
        $results = $this->getResults();
        $pdo->shouldReceive('prepare')->once()->with('SELECT * FROM something')->andReturn($stmt);
        $stmt->shouldReceive('execute')->once()->with(['foo' => 'bar']);
        $stmt->shouldReceive('fetch')->andReturnValues($results);
        $stmt->shouldReceive('closeCursor')->once();
        foreach ($sut->read(['foo' => 'bar']) as $i => $d) {
            $this->assertInstanceOf(Data::class, $d);
            $this->assertEquals($results[$i], $d->toArray());
        }
        $this->assertEquals(1, $i);
    }

    public function testReadNoInteractionIsException()
    {
        list($sut, $pdo, $stmt) = $this->getSut(null);
        $this->expectException(ActionException::class);
        $this->expectExceptionMessage('No interaction provided.');
        foreach ($sut->read() as $i => $d) {}
    }

    protected function getResults()
    {
        return [
            [
                'foo' => 'bar 1',
                'biz' => 'buz 1',
            ],
            [
                'foo' => 'bar 2',
                'biz' => 'buz 2',
            ],
            false,
        ];
    }

    protected function getSut($interaction = 'SELECT * FROM something')
    {
        $pdo = m::mock(\PDO::class);
        $stmt = m::mock(\PDOStatement::class);
        $handlers = [new NullHandler()];
        $l = new Logger((new \ReflectionClass($this))->getShortName(), $handlers);
        $b = new PDO($l, $pdo);
        $sut = new Read($l, $b, $interaction);
        return [$sut, $pdo, $stmt];
    }
}