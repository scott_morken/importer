<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 11:35 AM
 */

namespace Tests\Importer\unit\Factories;

use Importer\Contracts\Backend;
use Importer\Data;
use Importer\Factories\Action;
use Mockery as m;
use Monolog\Handler\NullHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Pimple\Container;
use Tests\Importer\Stubs\Actions\Dummy\Execute;
use Tests\Importer\Stubs\Actions\Dummy\Read;
use Tests\Importer\Stubs\Actions\Dummy\Write;

class ActionTest extends TestCase
{

    public function testCreateReader()
    {
        $config = $this->readerConfig();
        list($sut, $c) = $this->getSut($config);
        $c['foo-backend'] = function ($c) {
            return m::mock(Backend::class);
        };
        $r = $sut->get('reader');
        $act = $r['act'];
        $this->assertInstanceOf(Read::class, $act);
    }

    public function testCreateReaderCanRead()
    {
        $config = $this->readerConfig();
        list($sut, $c) = $this->getSut($config);
        $b = m::mock(Backend::class);
        $c['foo-backend'] = function ($c) use ($b) {
            return $b;
        };
        $b->shouldReceive('read')
          ->andReturn($this->dummyData(3));
        $r = $sut->get('reader');
        $act = $r['act'];
        foreach ($act->read() as $i => $data) {
            $this->assertEquals('a ' . ($i + 1), $data->get('a'));
            $this->assertEquals('b ' . ($i + 1), $data->get('b'));
            $this->assertEquals('c ' . ($i + 1), $data->get('c'));
        }
        $this->assertEquals(2, $i);
    }

    public function testCreateWriters()
    {
        $config = $this->writerConfig();
        list($sut, $c) = $this->getSut($config);
        $b = m::mock(Backend::class);
        $c['foo-backend'] = function ($c) use ($b) {
            return $b;
        };
        $r = $sut->get('writers');
        $foowrite = $r['foo_write'];
        $this->assertInstanceOf(Execute::class, $foowrite['pre']);
        $this->assertInstanceOf(Write::class, $foowrite['act']);
        $this->assertNull($foowrite['post']);
    }

    public function testCreateSingleWriter()
    {
        $config = $this->writerConfig();
        list($sut, $c) = $this->getSut($config);
        $b = m::mock(Backend::class);
        $c['foo-backend'] = function ($c) use ($b) {
            return $b;
        };
        $foowrite = $sut->get('writers.foo_write');
        $this->assertInstanceOf(Execute::class, $foowrite['pre']);
        $this->assertInstanceOf(Write::class, $foowrite['act']);
        $this->assertNull($foowrite['post']);
    }

    public function testCreateSingleWriterCanAct()
    {
        $config = $this->writerConfig();
        list($sut, $c) = $this->getSut($config);
        $b = m::mock(Backend::class);
        $c['foo-backend'] = function ($c) use ($b) {
            return $b;
        };
        $data = array_map(
            function ($v) {
                return new Data($v);
            },
            $this->dummyData(3)
        );
        $foowrite = $sut->get('writers.foo_write');
        $pre = $foowrite['pre'];
        $act = $foowrite['act'];
        $this->assertEquals('Executing: TRUNCATE table_foo', $pre->execute());
        $this->assertEquals(3, $act->write($data));
    }

    public function testCreateSingleWriterCallsMethods()
    {
        $config = $this->writerConfig();
        $config['writers']['foo_write']['pre']['methods'] = [
            'modifyInteraction' => ['abc'],
        ];
        list($sut, $c) = $this->getSut($config);
        $b = m::mock(Backend::class);
        $c['foo-backend'] = function ($c) use ($b) {
            return $b;
        };
        $data = array_map(
            function ($v) {
                return new Data($v);
            },
            $this->dummyData(3)
        );
        $foowrite = $sut->get('writers.foo_write');
        $pre = $foowrite['pre'];
        $this->assertEquals('Executing: TRUNCATE table_foo abc', $pre->execute());
    }

    protected function readerConfig()
    {
        return [
            'reader' => [
                'backend' => 'foo-backend',
                'pre'     => [],
                'act'     => [
                    'cls' => Read::class,
                ],
                'post'    => [],
            ],
        ];
    }

    protected function writerConfig()
    {
        return [
            'writers' => [
                'foo_write' => [
                    'backend' => 'foo-backend',
                    'pre'     => [
                        'cls'         => Execute::class,
                        'interaction' => 'TRUNCATE table_foo',
                    ],
                    'act'     => [
                        'cls'         => Write::class,
                        'interaction' => null,
                        'methods'     => [
                            'setMap' => [
                                [
                                    'cola' => 'a',
                                    'colb' => function (\Importer\Contracts\Data $data) {
                                        return $data->get('b') . ' closure';
                                    },
                                ],
                            ],
                        ],
                    ],
                    'post'    => [],
                ],
            ],
        ];
    }

    protected function dummyData($count)
    {
        $ret = [];
        for ($i = 1; $i <= $count; $i++) {
            $ret[] = [
                'a' => 'a ' . $i,
                'b' => 'b ' . $i,
                'c' => 'c ' . $i,
            ];
        }
        return $ret;
    }

    protected function getSut($config = [])
    {
        $c = new Container();
        $handlers = [new NullHandler()];
        $l = new Logger((new \ReflectionClass($this))->getShortName(), $handlers);
        $sut = new Action($c, $l, $config);
        return [$sut, $c];
    }
}