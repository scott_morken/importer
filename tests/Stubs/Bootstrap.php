<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 2:13 PM
 */

namespace Tests\Importer\Stubs;

class Bootstrap extends \Importer\Bootstrap
{

    protected function registerPaths()
    {
        $base = realpath(__DIR__ . sprintf('%s..%s', DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR));
        $this->container['base_path'] = $base;
        $this->container['config_path'] = realpath($base . DIRECTORY_SEPARATOR . 'config');
        $this->container['storage_path'] = realpath($base . DIRECTORY_SEPARATOR . 'storage');
    }
}