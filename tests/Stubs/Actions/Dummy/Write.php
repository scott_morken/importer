<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:54 AM
 */

namespace Tests\Importer\Stubs\Actions\Dummy;

class Write extends \Importer\Actions\Write implements \Importer\Contracts\Actions\Write
{

    protected $interaction_required = false;

    /**
     * @param array $data
     * @return int
     */
    public function write(array $data)
    {
        $count = 0;
        foreach ($data as $d) {
            $put = $this->mapColumns($d, $this->getMap());
            $count++;
        }
        return $count;
    }
}