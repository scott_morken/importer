<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 12:00 PM
 */

namespace Tests\Importer\Stubs\Actions\Dummy;

use Importer\Actions\Base;

class Execute extends Base implements \Importer\Contracts\Actions\Execute
{

    protected $interaction_required = true;

    /**
     * @param array $params
     * @return mixed
     */
    public function execute($params = [])
    {
        return 'Executing: ' . $this->getInteraction();
    }

    public function modifyInteraction($mod)
    {
        $this->interaction .= ' ' . $mod;
    }
}