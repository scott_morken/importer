<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 7:49 AM
 */

namespace Tests\Importer\Stubs\Actions\Dummy;

class Read extends \Importer\Actions\Read implements \Importer\Contracts\Actions\Read
{

    protected $interaction_required = false;

    /**
     * @param array $params
     * @return \Importer\Contracts\Data[]
     */
    public function read($params = [])
    {
        foreach ($this->getBackend()->read() as $d) {
            yield $this->getData($d);
        }
    }
}