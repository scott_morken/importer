<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 1:13 PM
 */
return [
    'actions' => [
        'reader'  => [
            'backend' => 'sqlite-read',
            'pre'     => [
                'cls'         => \Importer\Actions\PDO\Execute::class,
                'interaction' => [
                    "CREATE TABLE IF NOT EXISTS bar (bar_col1 NOT NULL, bar_col2 NOT NULL)",
                    "INSERT INTO bar (bar_col1, bar_col2) VALUES('c1 v1', 'c2 v1'),('c1 v2', 'c2 v2'),('c1 v3', 'c2 v3'),('c1 v4', 'c2 v4')",
                ],
            ],
            'act'     => [
                'cls'         => \Importer\Actions\PDO\Read::class,
                'interaction' => 'SELECT * FROM bar',
            ],
            'post'    => [],
        ],
        'writers' => [
            'sqlite_write' => [
                'backend' => 'sqlite-write',
                'pre'     => [
                    'cls'         => \Importer\Actions\PDO\Execute::class,
                    'interaction' => [
                        'CREATE TABLE IF NOT EXISTS foo (foo_col1 NOT NULL, foo_col2 NOT NULL)',
                        'DELETE FROM foo',
                    ],
                ],
                'act'     => [
                    'cls'         => \Importer\Actions\PDO\Write::class,
                    'interaction' => 'INSERT INTO foo (foo_col1, foo_col2)',
                ],
                'post'    => [],
            ],
            'csv_write'    => [
                'backend' => 'csv-write',
                'pre'     => [],
                'act'     => [
                    'cls'     => \Importer\Actions\CSV\Write::class,
                    'methods' => [
                        'setMap' => [
                            [
                                'foo_col1' => 'bar_col1',
                                'foo_col2' => function (\Importer\Contracts\Data $data) {
                                    return $data->get('bar_col2') . ' abc';
                                },
                            ],
                        ],
                    ],
                ],
                'post'    => [],
            ],
        ],
    ],
];