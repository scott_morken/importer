<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 1:14 PM
 */
return [
    'backends' => [
        'sqlite-read'  => [
            'cls'  => \Importer\Backends\PDO::class,
            'args' => [
                ['connection' => 'sqlite'],
            ],
        ],
        'sqlite-write' => [
            'cls'  => \Importer\Backends\PDO::class,
            'args' => [
                ['connection' => 'sqlite'],
            ],
        ],
        'csv-write'    => [
            'cls'  => \Importer\Backends\Csv::class,
            'args' => [
                'path' => __DIR__ . '/../storage/write.csv',
            ],
        ],
    ],
];