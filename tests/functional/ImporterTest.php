<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/30/17
 * Time: 2:12 PM
 */

namespace Tests\Importer\functional;

use Importer\Importer;
use PHPUnit\Framework\TestCase;
use Pimple\Container;
use Psr\Log\LoggerInterface;
use Tests\Importer\Stubs\Bootstrap;

class ImporterTest extends TestCase
{

    /**
     * @var \Pimple\Container
     */
    protected $container;

    public function setUp()
    {
        parent::setUp();
        $storage = __DIR__ . '/../storage';
        if (file_exists($storage . '/importer.log')) {
            unlink($storage . '/importer.log');
        }
        if (file_exists($storage . '/write.csv')) {
            unlink($storage . '/write.csv');
        }
    }

    public function testBootstrapCorrectPaths()
    {
        $c = $this->getContainer();
        $this->bootstrap($c);
        $this->assertStringEndsWith('tests/config', $c['config_path']);
        $this->assertStringEndsWith('tests/storage', $c['storage_path']);
    }

    public function testImporterRun()
    {
        $sut = $this->getSut();
        $results = $sut->run(3);
        $pre = $results->pre;
        $this->assertCount(2, $pre['reader']);
        $this->assertCount(2, $pre['writers.sqlite_write']);
        $act = $results->act;
        $this->assertEquals(4, $act['reader']);
        $this->assertEquals(4, $act['writers.sqlite_write']);
        $this->assertEquals(4, $act['writers.csv_write']);
        $csv = explode("\n", file_get_contents(__DIR__ . '/../storage/write.csv'));
        $this->assertEquals('"c1 v1","c2 v1 abc"', $csv[0]);
        $this->assertEquals('"c1 v2","c2 v2 abc"', $csv[1]);
        $this->assertEquals('"c1 v3","c2 v3 abc"', $csv[2]);
        $this->assertEquals('"c1 v4","c2 v4 abc"', $csv[3]);
    }

    protected function getSut()
    {
        $c = $this->getContainer();
        $this->bootstrap($c);
        return new Importer($c, $c[LoggerInterface::class]);
    }

    protected function bootstrap($container)
    {
        return Bootstrap::register($container);
    }

    protected function getContainer()
    {
        if (!$this->container) {
            $this->container = new Container();
        }
        return $this->container;
    }
}