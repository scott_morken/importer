<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 10:49 AM
 */

require_once __DIR__ . DIRECTORY_SEPARATOR . 'bootstrap/bootstrap.php';

$logger = $container[\Psr\Log\LoggerInterface::class];

$importer = new \Importer\Importer($container, $logger);

/*
 * Wrap the run command in the service to try to
 * keep only one instance running at a time
 */
$service = $container[\Importer\Contracts\Service::class];
$service->start();

$results = $importer->run();

$service->stop();

/*
 * Encode the results and log them at the notice level
 * so that the emailed results can be set to notice.
 * Print_r and a2str end up poorly formatted, hence json
 */
$logger->notice(json_encode($results->toArray()));