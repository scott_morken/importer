<?php
/**
 * Created by PhpStorm.
 * User: scoce95461
 * Date: 11/29/17
 * Time: 10:50 AM
 */

require_once __DIR__ . '/autoload.php';

$container = new \Pimple\Container();

\Importer\Bootstrap::register($container);

if (file_exists(__DIR__ . '/container.php')) {
    require_once __DIR__ . '/container.php';
}