#### Docker Ant Build container

* copy `project/vendor/smorken/docker/docker.build.copy` to `project/docker.build`

Run `ant min`
```
$ cd docker.build
$ docker-compose up
```

Run `ant [arg]` - replace \[arg\] with a build target
```
$ cd docker.build
$ docker-compose run builder phploc
```

Full build
```
$ cd docker.build
$ docker-compose run builder build
```

__Note__: iconv in Alpine Linux is not compatible with phpdox's //TRANSLIT so the
phpdox target will not run in this container.
