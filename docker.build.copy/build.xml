<project name="importer" default="build" basedir="/app">
    <!--  By default, we assume all tools to be on the $PATH  -->
    <property name="toolsdir" value=""/>
    <property name="builddir" value="${basedir}/docker.build/build"/>
    <property environment="env"/>
    <!-- set up some defaults, should not be overwritten if already set -->
    <property name="env.DOCKER_BUILDER" value="0"/>
    <property name="env.WORKSPACE" value="/root"/>
    <property name="env.JOB_NAME" value="${ant.project.name}"/>
    <property name="env.BUILD_NUMBER" value="1"/>
    <!--
     Uncomment the following when the tools are in ${basedir}/vendor/bin
    -->
    <!--
     <property name="toolsdir" value="${basedir}/vendor/bin/"/>
    -->
    <target name="min" depends="prepare,copy,composer,tarballs,zips,move-archives" description=""/>
    <target name="build" depends="prepare,copy,composer,lint,phploc-ci,pdepend,phpmd-ci,phpcs-ci,phpcpd-ci,phpunit,phpdox,tarballs,zips,move-archives" description=""/>
    <target name="build-parallel" depends="prepare,copy,composer,lint,tools-parallel,phpunit,phpdox,tarballs,zips,move-archives" description=""/>
    <target name="tools-parallel" description="Run tools in parallel">
        <parallel threadCount="2">
            <sequential>
                <antcall target="pdepend"/>
                <antcall target="phpmd-ci"/>
            </sequential>
            <antcall target="phpcpd-ci"/>
            <antcall target="phpcs-ci"/>
            <antcall target="phploc-ci"/>
        </parallel>
    </target>
    <target name="clean" unless="clean.done" description="Cleanup build artifacts">
        <delete dir="${builddir}/api"/>
        <delete dir="${builddir}/coverage"/>
        <delete dir="${builddir}/logs"/>
        <delete dir="${builddir}/pdepend"/>
        <delete dir="${builddir}/phpdox"/>
        <property name="clean.done" value="true"/>
    </target>
    <target name="prepare" unless="prepare.done" depends="clean" description="Prepare for build">
        <mkdir dir="${builddir}/api"/>
        <mkdir dir="${builddir}/coverage"/>
        <mkdir dir="${builddir}/logs"/>
        <mkdir dir="${builddir}/pdepend"/>
        <mkdir dir="${builddir}/phpdox"/>
        <property name="prepare.done" value="true"/>
    </target>
    <target name="lint" unless="lint.done" description="Perform syntax check of sourcecode files">
        <apply executable="php" failonerror="true" taskname="lint">
            <arg value="-l"/>
            <fileset dir="${basedir}/src">
                <include name="**/*.php"/>
                <modified/>
            </fileset>
            <fileset dir="${basedir}/config">
                <include name="**/*.php"/>
                <modified/>
            </fileset>
            <fileset dir="${basedir}/tests">
                <include name="**/*.php"/>
                <modified/>
            </fileset>
        </apply>
        <property name="lint.done" value="true"/>
    </target>
    <target name="phploc" unless="phploc.done" description="Measure project size using PHPLOC and print human readable output. Intended for usage on the command line.">
        <exec executable="${toolsdir}phploc" taskname="phploc">
            <arg value="--count-tests"/>
            <arg path="${basedir}/src"/>
            <arg path="${basedir}/config"/>
            <arg path="${basedir}/tests"/>
        </exec>
        <property name="phploc.done" value="true"/>
    </target>
    <target name="phploc-ci" unless="phploc.done" depends="prepare" description="Measure project size using PHPLOC and log result in CSV and XML format. Intended for usage within a continuous integration environment.">
        <exec executable="${toolsdir}phploc" taskname="phploc">
            <arg value="--count-tests"/>
            <arg value="--log-csv"/>
            <arg path="${builddir}/logs/phploc.csv"/>
            <arg value="--log-xml"/>
            <arg path="${builddir}/logs/phploc.xml"/>
            <arg path="${basedir}/src"/>
            <arg path="${basedir}/config"/>
            <arg path="${basedir}/tests"/>
        </exec>
        <property name="phploc.done" value="true"/>
    </target>
    <target name="pdepend" unless="pdepend.done" depends="prepare" description="Calculate software metrics using PHP_Depend and log result in XML format. Intended for usage within a continuous integration environment.">
        <exec executable="${toolsdir}pdepend" taskname="pdepend">
            <arg value="--jdepend-xml=${builddir}/logs/jdepend.xml"/>
            <arg value="--jdepend-chart=${builddir}/pdepend/dependencies.svg"/>
            <arg value="--overview-pyramid=${builddir}/pdepend/overview-pyramid.svg"/>
            <arg path="${basedir}/src,${basedir}/config"/>
        </exec>
        <property name="pdepend.done" value="true"/>
    </target>
    <target name="phpmd" unless="phpmd.done" description="Perform project mess detection using PHPMD and print human readable output. Intended for usage on the command line before committing.">
        <exec executable="${toolsdir}phpmd" taskname="phpmd">
            <arg path="${basedir}/src,${basedir}/config"/>
            <arg value="text"/>
            <arg path="${builddir}/phpmd.xml"/>
        </exec>
        <property name="phpmd.done" value="true"/>
    </target>
    <target name="phpmd-ci" unless="phpmd.done" depends="prepare" description="Perform project mess detection using PHPMD and log result in XML format. Intended for usage within a continuous integration environment.">
        <exec executable="${toolsdir}phpmd" taskname="phpmd">
            <arg path="${basedir}/src,${basedir}/config"/>
            <arg value="xml"/>
            <arg path="${builddir}/phpmd.xml"/>
            <arg value="--reportfile"/>
            <arg path="${builddir}/logs/pmd.xml"/>
        </exec>
        <property name="phpmd.done" value="true"/>
    </target>
    <target name="phpcs" unless="phpcs.done" description="Find coding standard violations using PHP_CodeSniffer and print human readable output. Intended for usage on the command line before committing.">
        <exec executable="${toolsdir}phpcs" taskname="phpcs">
            <arg value="--standard=${builddir}/phpcs.xml"/>
            <arg value="--extensions=php"/>
            <arg value="--ignore=autoload.php"/>
            <arg path="${basedir}/src"/>
            <arg path="${basedir}/config"/>
        </exec>
        <property name="phpcs.done" value="true"/>
    </target>
    <target name="phpcbf" description="Fix coding standard violations using PHP_CodeSniffer and print human readable output. Intended for usage on the command line before committing.">
        <exec executable="${toolsdir}phpcbf" taskname="phpcbf">
            <arg value="--standard=${builddir}/phpcs.xml"/>
            <arg value="--extensions=php"/>
            <arg value="--ignore=autoload.php"/>
            <arg path="${basedir}/src"/>
            <arg path="${basedir}/config.copy"/>
        </exec>
    </target>
    <target name="phpcs-ci" unless="phpcs.done" depends="prepare" description="Find coding standard violations using PHP_CodeSniffer and log result in XML format. Intended for usage within a continuous integration environment.">
        <exec executable="${toolsdir}phpcs" output="/dev/null" taskname="phpcs">
            <arg value="--report=checkstyle"/>
            <arg value="--report-file=${builddir}/logs/checkstyle.xml"/>
            <arg value="--standard=${builddir}/phpcs.xml"/>
            <arg value="--extensions=php"/>
            <arg value="--ignore=autoload.php"/>
            <arg path="${basedir}/src"/>
            <arg path="${basedir}/config"/>
        </exec>
        <property name="phpcs.done" value="true"/>
    </target>
    <target name="phpcpd" unless="phpcpd.done" description="Find duplicate code using PHPCPD and print human readable output. Intended for usage on the command line before committing.">
        <exec executable="${toolsdir}phpcpd" taskname="phpcpd">
            <arg path="${basedir}/src"/>
        </exec>
        <property name="phpcpd.done" value="true"/>
    </target>
    <target name="phpcpd-ci" unless="phpcpd.done" depends="prepare" description="Find duplicate code using PHPCPD and log result in XML format. Intended for usage within a continuous integration environment.">
        <exec executable="${toolsdir}phpcpd" taskname="phpcpd">
            <arg value="--log-pmd"/>
            <arg path="${builddir}/logs/pmd-cpd.xml"/>
            <arg path="${basedir}/src"/>
            <arg path="${basedir}/config"/>
        </exec>
        <property name="phpcpd.done" value="true"/>
    </target>
    <target name="phpunit" unless="phpunit.done" depends="prepare" description="Run unit tests with PHPUnit">
        <exec executable="${toolsdir}phpunit" failonerror="true" taskname="phpunit">
            <arg value="--configuration"/>
            <arg path="${basedir}/phpunit.xml"/>
            <arg value="--coverage-clover"/>
            <arg path="${builddir}/logs/clover.xml"/>
            <arg value="--coverage-crap4j"/>
            <arg path="${builddir}/logs/crap4j.xml"/>
            <arg value="--log-junit"/>
            <arg path="${builddir}/logs/junit.xml"/>
        </exec>
        <property name="phpunit.done" value="true"/>
    </target>
    <target name="phpdox" unless="phpdox.done" depends="phpdox.run,phploc-ci,phpcs-ci,phpmd-ci" description="Generate project documentation using phpDox">
        <exec executable="${toolsdir}phpdox" dir="${builddir}/build" taskname="phpdox"/>
        <property name="phpdox.done" value="true"/>
    </target>
    <target name="phpdox.run">
        <condition property="phpdox.done">
            <or>
                <istrue value="${phpdox.done}"/>
                <equals arg1="${env.DOCKER_BUILDER}" arg2="1"/>
            </or>
        </condition>
        <echo message="phpdox.done: ${phpdox.done}."/>
    </target>
    <target name="copy" unless="copy.done" description="Copy gitignored to live">
        <copy todir="${basedir}/config">
            <fileset dir="${basedir}/config.copy" />
        </copy>
        <property name="copy.done" value="true"/>
    </target>
    <target name="clean-composer" description="Cleanup composer artifacts">
        <echo>Cleaning out the composer artifacts</echo>
        <delete dir="${basedir}/vendor" />
        <delete file="${basedir}/composer.lock" />
    </target>
    <target name="composer" depends="composer-update" description="Install or update dependencies" />
    <target name="composer-update" description="Updating dependencies">
        <echo>Installing/Updating Composer dependencies</echo>
        <exec executable="composer" failonerror="true">
            <arg value="update" />
            <arg value="--no-scripts" />
            <arg value="--ignore-platform-reqs" />
        </exec>
    </target>
    <target name="tarballs" description="Build release tarballs">
        <property name="tarballs.base" value="${env.WORKSPACE}/../../releases/${env.JOB_NAME}/${env.JOB_NAME}-${env.BUILD_NUMBER}"/>
        <echo message="Building release tarballs to ${tarballs.base}."/>
        <tar longfile="gnu" destfile="${tarballs.base}.tar.gz" basedir="${basedir}"
             excludes="config/**,docker*/"
             compression="gzip">
        </tar>
        <tar longfile="gnu" destfile="${tarballs.base}-dev.tar.gz" basedir="${basedir}"
             excludes="docker*/"
             compression="gzip">
        </tar>
    </target>
    <target name="zips" description="Build release zips">
        <property name="zips.base" value="${env.WORKSPACE}/../../releases/${env.JOB_NAME}/${env.JOB_NAME}-${env.BUILD_NUMBER}"/>
        <echo message="Building release zips to ${zips.base}."/>
        <zip destfile="${zips.base}.zip" basedir="${basedir}"
             excludes="config/**,docker*/">
        </zip>
        <zip destfile="${zips.base}-dev.zip" basedir="${basedir}"
             excludes="docker*/">
        </zip>
    </target>
    <target name="move-archives" if="${env.DOCKER_BUILDER}" description="Move archived files" depends="tarballs,zips">
        <echo message="Moving archive files from /releases to /builds"/>
        <move todir="/builds">
            <fileset dir="/releases">
                <include name="**/*.zip"/>
                <include name="**/*.gz"/>
            </fileset>
        </move>
    </target>
</project>
